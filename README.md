# Install:

```
    yarn add "https://shinneider@bitbucket.org/shinneider/pc-vue-template.git#master"
```

# Usage

1.  Import icons you want:

        import *PluginHere* from "pc-vue-template/dist/*PluginHere*";

1.  Import Css:

        <style> /* Import in main app only */
        @import '~pc-vue-template/dist/*PluginHere*';
        </style>
        /* Or */
        <style scoped> /* Import in each app */
        @import '~pc-vue-template/dist/*PluginHere*';
        </style>

import SimpleFooter from "./SimpleFooter";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

export default {
  title: "Footer/SimpleFooter",
  component: SimpleFooter,
  argTypes: {
    size: {
      control: { type: "select", options: ["xs", "sm", "md", "lg", "xlg"] },
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { SimpleFooter },
  template: '<SimpleFooter v-bind="$props" />',
});

export const Default = Template.bind({});
Default.args = {
  siteName: "Site Here",
  enterpriseName: "Enterprise here",
};

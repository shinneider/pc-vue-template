import PriceCard from "./PricingCard";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { gridAvailableSizes } from "pc-vue-basic/dist/gridValidators";

export default {
  title: "Cards/PriceCard",
  component: PriceCard,
  argTypes: {
    popular: { control: "boolean" },
    colXs: {
      control: { type: "select", options: gridAvailableSizes },
    },
    colSm: {
      control: { type: "select", options: gridAvailableSizes },
    },
    colMd: {
      control: { type: "select", options: gridAvailableSizes },
    },
    colLg: {
      control: { type: "select", options: gridAvailableSizes },
    },
    colXl: {
      control: { type: "select", options: gridAvailableSizes },
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PriceCard },
  template: '<PriceCard v-bind="$props" />',
});

const TemplateDark = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PriceCard },
  template:
    '<section class="dark" style="max-width: 100%; background-color: black; padding: 5px;"> <PriceCard v-bind="$props" /> </section>',
});

export const Default = Template.bind({});
Default.args = {
  text: "text here",
  title: "title here",
  price: "25.00",
  popular: false,
  colXs: "4",
};

export const Popular = Template.bind({});
Popular.args = {
  text: "text here",
  title: "title here",
  price: "25.00",
  popular: true,
  colXs: "4",
};

export const InDarkSection = TemplateDark.bind({});
InDarkSection.args = {
  text: "text here",
  title: "title here",
  price: "25.00",
  popular: false,
  colXs: "4",
};
